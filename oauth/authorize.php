<?php
session_start();

/**
 * @author Denis CLAVIER <clavierd at gmail dot com>
 * Adapted from Oauth2-server-php cookbook
 * @see http://bshaffer.github.io/oauth2-server-php-docs/cookbook/
 */

// include our OAuth2 Server object
require_once __DIR__.'/server.php';

$request = OAuth2\Request::createFromGlobals();
$response = new OAuth2\Response();

// validate the authorize request
if (!$server->validateAuthorizeRequest($request, $response)) {
    $response->send();
    die;
}

// if user is not yet authenticated, he is redirected.
if (!isset($_SESSION['uid']))
{
  //store the authorize request
  $explode_url=explode("/", strip_tags(trim($_SERVER['REQUEST_URI']))); 
  $_SESSION['auth_page']=end($explode_url);
  header('Location: index.php');
  exit();
}

// print the authorization code if the user has authorized your client
$is_authorized = true;
$server->handleAuthorizeRequest($request, $response, $is_authorized,$_SESSION['uid']);

if ($is_authorized) 
{
  // This is only here so that you get to see your code in the cURL request. Otherwise, we'd redirect back to the client
  $code = substr($response->getHttpHeader('Location'), strpos($response->getHttpHeader('Location'), 'code=')+5, 40);
  header('Location: ' . $response->getHttpHeader('Location'));
  exit();
}

// Send message in case of error
$response->send();
