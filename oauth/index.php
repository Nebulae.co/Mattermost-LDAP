<?php
session_start();
/**
 * @author Denis CLAVIER <clavierd at gmail dot com>
 * A modified verion by dimst23
 */


// include our LDAP object
require_once __DIR__.'/LDAP/LDAP.php';
require_once __DIR__.'/LDAP/config_ldap.php';

$prompt_template = new DOMDocument();
$prompt_template->loadHTMLFile('form_prompt.html');


function messageShow($html_template, $message = 'No Msg') {
	$modification_node = $html_template->getElementById('error_message');
	$page_fragment = $html_template->createDocumentFragment();
	$page_fragment->appendXML($message);

	$modification_node->appendChild($page_fragment);

	echo $html_template->saveHTML();
}

if (empty($_POST)) {
	messageShow($prompt_template, '');
} else {
	// Verify all fields have been filled 
	if (empty($_POST['user']) || empty($_POST['password'])) 
	{
		if (empty($_POST['user'])) {
			messageShow($prompt_template, 'Merci d\'entrer votre nom d\'utilisateur·trice');
		} else {
			messageShow($prompt_template, 'Merci d\'entrer votre mot de passe.');
		}
	}
	else
	{
		// Remove every html tag and useless space on username (to prevent XSS)
		$user=strip_tags(trim($_POST['user']));
		$password=strip_tags($_POST['password']);

		// Open a LDAP connection
		$ldap = new LDAP($ldap_host,$ldap_port,$ldap_version);
		
		// Check user credential on LDAP
		try{
			$authenticated = $ldap->checkLogin($user,$password,$ldap_search_attribute,$ldap_filter,$ldap_base_dn,$ldap_bind_dn,$ldap_bind_pass);
		}
		catch (Exception $e)
		{
			$authenticated = false;
		}

		// If user is authenticated
		if ($authenticated)
		{
			$_SESSION['uid']=$user;

			// If user came here with an autorize request, redirect him to the authorize page. Else prompt a simple message.
			if (isset($_SESSION['auth_page']))
			{
				$auth_page=$_SESSION['auth_page'];
				header('Location: ' . $auth_page);
				exit();
			}
			else 
			{
				messageShow($prompt_template, 'Une erreur est survenue... Veuillez contacter un·e administrateur·trice.');
			}
			}
			// check login on LDAP has failed. Login and password were invalid or LDAP is unreachable
		else 
		{
			messageShow($prompt_template, 'Connexion echouée. Vérifiez votre nom d\'utilisateur·trice et votre mot de passe. Si le problème persiste, contactez un·e administrateur·trice');
		}
	}
}